public class Resta extends Calculo{
    
    double resta;
       
    public Resta(double A, double B) {
             
        super(A, B, '-');
        this.resta = A-B;
        this.setResultado(this.resta);
    }
}