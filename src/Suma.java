public class Suma extends Calculo{
    double suma;
       
    public Suma(double A, double B) {
             
        super(A, B, '+');
        this.suma = A + B;
        this.setResultado(this.suma);
    }
}